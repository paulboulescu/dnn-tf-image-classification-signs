# CNN Image Classifier with TensorFlow

## About
Deep Neural Network (fully connected layers) model with TensorFlow, used to classify images of 6 hand signs, corresponding to numbers from 0 to 5.

## Instalation
1. Clone the repository
```
$ git clone https://paulboulescu@bitbucket.org/paulboulescu/dnn-tf-image-classification-signs.git
```

2. Train and Test
```
$ python model.py
```

## Requirements
1. TensorFlow

## Details
* Model: LINEAR -> RELU -> LINEAR -> RELU -> LINEAR -> SOFTMAX
* Train Accuracy: **0.991666**
* Test Accuracy: **0.741666**
* Epochs: **1500**
* MiniBatch: **32**
* Learning Rate: **0.0001**
* Optimizer: **Adam**
* Initializer: **Xavier**
* Loss: **Softmax / Cross Entropy**

## Disclaimer
Created under the Deep Learning Specialization (Andrew Ng, Younes Bensouda Mourri, Kian Katanforoosh) - Coursera. The code uses ideas, datasets, algorithms, and code fragments presented in the Course.